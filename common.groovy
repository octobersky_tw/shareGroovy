#!/usr/bin/env groovy

def checkoutSource() {
    try {
        checkout scm
    }
    catch (error) {
        notifySCMFail()
        throw error
    }
}

def notifySCMFail() {
    def colorCode = '#D00000'  // color red
    def projectMsg = "Project Name: ${env.JOB_NAME}"
    def resultMsg = "Result: Fail to get source code\nJob-URL: ${env.JOB_URL}"

    def msg = "${projectMsg}\n\n${resultMsg}"

    slackSend(color: colorCode, message: msg)
}

def notifyResult(String result) {
    def colorCode
    def projectMsg = "Project Name: ${env.JOB_NAME}"
    def resultMsg = "Result: ${result}\nJob-URL: ${env.JOB_URL}\n${env.BUILD_DISPLAY_NAME} Build-URL: ${env.BUILD_URL}"
    def gitMsg = sh returnStdout: true,
                    script: 'git log -1 --pretty=format:"Author: %an%nCommiter: %cn%nCommit Message: %s%nCommit ID: %h"'
    def msg = "${projectMsg}\n\n${resultMsg}\n\n${gitMsg}"

    if (result == 'SUCCESS') {
        colorCode = '#36A64F'  // color green
    } else if (result == 'FAILURE') {
        colorCode = '#D00000'  // color red
    } else if (result == 'UNSTABLE') {
        colorCode = '#FFB518'  // color yellow
    }

    slackSend(color: colorCode, message: msg)
}

return this // must be added
