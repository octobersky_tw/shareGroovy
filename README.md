# Introduction

The repository contains the common groovy code among different Jenkinsfile and avoids code duplication.


# How to use in other Jenkinsfile

```
// The variable for storing the common groovy code
def common

node {
    git 'git@tainan.tispace.com:tools/shareGroovy.git'
    common = load 'common.groovy'
}
```

Take `checkoutSource()` function as an example:

```
stage('Checkout Source Code') {
    node {
        common.checkoutSource()
    }
}
```
